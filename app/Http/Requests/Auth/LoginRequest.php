<?php

namespace App\Http\Requests\Auth;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'sometimes|email',
            'phone' => 'sometimes|regex:/^\+\d{10,16}$/',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => 'errors.register.email-email',
            'phone.sometimes' => 'errors.register.phone-required',
            'phone.regex' => 'errors.register.phone-regex',
            'captcha.required' => 'errors.register.captcha-required'
        ];
    }
}
