<?php

namespace App\Http\Middleware;

use Closure;

class RaDataSimpleRestResponse {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $content = $response->getOriginalContent();

        if ($content instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $response->header('Content-Range', (int)$content->firstItem() . '-' . (int)$content->lastItem() . '/' . $content->total());
            $response->setContent($content->getCollection());
        }

        if ($content instanceof \Illuminate\Database\Eloquent\Builder) {
            $query = $content->getQuery();
            $response->header('Content-Range', (int)$query->offset . '-' . (int)$query->limit . '/' . $query->count());
        }

        return $response;
    }
}