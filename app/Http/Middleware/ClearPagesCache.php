<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Storage;

class ClearPagesCache {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        $response = $next($request);

        if (in_array($request->method(), ['POST', 'PUT', 'PATCH'])) {
            if (Storage::exists('pagesCache')) {
                foreach (Storage::files('pagesCache') as $filePath) {
                    Storage::delete($filePath);
                };
            }
        }

        return $response;
    }
}
