<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\File;

class LfmAutocreate {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('working_dir', null) &&
            $request->input('type', null)
        ) {
            $dir = null;
            $sharedFolder = stristr($request->input('working_dir'),config('lfm.shared_folder_name'));
            switch ($request->input('type')) {
                case 'Images':
                    if ($sharedFolder) {
                        $dir = base_path(config('lfm.disk') . '/' . config('lfm.folder_categories.image.folder_name') . $request->input('working_dir'));
                    } elseif (!$sharedFolder) {
                        $dir = base_path(config('lfm.disk') . '/' . config('lfm.folder_categories.image.folder_name') . '/' . config('lfm.shared_folder_name') . $request->input('working_dir'));
                    }
                    break;

                case 'Videos':
                    if ($sharedFolder) {
                        $dir = base_path(config('lfm.disk') . '/' . config('lfm.folder_categories.file.folder_name') . $request->input('working_dir'));
                    } elseif (!$sharedFolder) {
                        $dir = base_path(config('lfm.disk') . '/' . config('lfm.folder_categories.file.folder_name') . '/' . config('lfm.shared_folder_name') . $request->input('working_dir'));
                    }
                    break;
            }
            if ($dir && !File::exists($dir) ) {
                File::makeDirectory($dir, config('lfm.create_folder_mode'), true, true);
            }
        }

        return $next($request);
    }
}
