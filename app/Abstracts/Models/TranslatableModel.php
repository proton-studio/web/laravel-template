<?php

namespace App\Abstracts\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

abstract class TranslatableModel extends Model implements TranslatableContract
{
    use Translatable;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->append('translations_array');
    }

    public function getTranslationsArrayAttribute() {

        return $this->getTranslationsArray();
    }
}
