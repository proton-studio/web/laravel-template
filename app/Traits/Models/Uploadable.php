<?php

namespace App\Traits\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

trait Uploadable
{
    static $images_type_to_extension = [
        IMAGETYPE_GIF => 'gif',
        IMAGETYPE_JPEG => 'jpg',
        IMAGETYPE_PNG => 'png'
    ];

    static $mimes_type_to_extension = [
        'application/pdf' => 'pdf'
    ];

    public function getUploadDirAttribute()
    {
        $date = new Carbon($this->created_at);

        return $this->table . DIRECTORY_SEPARATOR . $date->format('Y') . DIRECTORY_SEPARATOR . $date->format('m');
    }


    protected function uploadedImageSet($image, $field)
    {
        if ($image) {
            if (isset($image['data'])) {
                if (preg_match('/^data:image\/\w+;base64,(.*)$/', $image['data'], $matches) && count($matches) == 2) {
                    $this->{$field . '_url'} = $this->saveImage($image, $field, $matches[1]);
                }
            }
        } else {
            $this->{$field . '_url'} = '';
        }
    }

    protected function uploadedImageGet($field)
    {
        if ($image = $this->{$field . '_url'}) {
            return $this->getImageByPath($image);
        } else {
            return null;
        }
    }

    protected function uploadedImagesSet($images, $field)
    {
        $data = [];

        if ($images) {
            $storage_images_urls = $this->{$field . '_urls'};

            if (!is_array($storage_images_urls)) {
                $storage_images_urls = [];
            }

            foreach ($storage_images_urls as $key => $file_path) {
                $storage_images_urls[$key] = Storage::disk(config('uploadable.disk'))->url($file_path);
            }

            foreach ($images as $key => $image) {
                if (isset($image['data'])) {
                    if (preg_match('/^data:image\/\w+;base64,(.*)$/', $image['data'], $matches) && count($matches) == 2) {
                        $data[$key] = $this->saveImage($image, $field, $matches[1]);
                    }
                } else {
                    $old_index = array_search($image['src'], $storage_images_urls);

                    if ($old_index !== false) {
                        $data[$key] = $this->{$field . '_urls'}[$old_index];
                    }
                }
            }
        }

        $this->{$field . '_urls'} = $data;
    }

    protected function uploadedImagesGet($field)
    {
        if ($images = $this->{$field . '_urls'}) {
            $data = [];

            foreach ($images as $image) {
                $data[] = $this->getImageByPath($image);
            }

            return $data;
        } else {
            return null;
        }
    }

    protected function uploadedFileSet($file, $field)
    {
        if ($file) {
            if (isset($file['data'])) {
                if (preg_match('/^data:application\/\w+;base64,(.*)$/', $file['data'], $matches) && count($matches) == 2) {
                    $this->{$field . '_url'} = $this->saveFile($file, $field, $matches[1]);
                }
            }
        } else {
            $this->{$field . '_url'} = '';
        }
    }

    protected function uploadedFileGet($field)
    {
        if ($file = $this->{$field . '_url'}) {
            return $this->getFileByPath($file);
        } else {
            return null;
        }
    }

    protected function uploadedFilesSet($files, $field)
    {
        $data = [];

        if ($files) {
            $storage_files_urls = $this->{$field . '_urls'};

            if (!is_array($storage_files_urls)) {
                $storage_files_urls = [];
            }

            foreach ($storage_files_urls as $key => $file_path) {
                $storage_files_urls[$key] = Storage::disk(config('uploadable.disk'))->url($file_path);
            }

            foreach ($files as $key => $file) {
                if (isset($file['data'])) {
                    if (preg_match('/^data:application\/\w+;base64,(.*)$/', $file['data'], $matches) && count($matches) == 2) {
                        $data[$key] = $this->saveFile($file, $field, $matches[1]);
                    }
                } else {
                    $old_index = array_search($file['src'], $storage_files_urls);

                    if ($old_index !== false) {
                        $data[$key] = $this->{$field . '_urls'}[$old_index];
                    }
                }
            }
        }

        $this->{$field . '_urls'} = $data;
    }

    protected function uploadedFilesGet($field)
    {
        if ($files = $this->{$field . '_urls'}) {
            $data = [];

            foreach ($files as $file) {
                $data[] = $this->getFileByPath($file);
            }

            return $data;
        } else {
            return null;
        }
    }

    private function saveImage($image, $field, $file_data)
    {
        $file_data = str_replace(' ', '+', $file_data);
        $img_data = base64_decode($file_data);
        list( , , $type) = getimagesizefromstring($img_data);

        try {
            if (isset(self::$images_type_to_extension[$type])) {
                $file_extension = self::$images_type_to_extension[$type];
            } else {
                throw new \Exception(trans('error.exceptions.invalid_image_type'));
            }

        } catch (\Exception $e) {
            Log::warning($e->getMessage());
            return null;
        }
        $dir_path = config('uploadable.uploads_dir', 'uploads') . DIRECTORY_SEPARATOR
            . config('uploadable.images_dir', 'images') . DIRECTORY_SEPARATOR
            . $this->upload_dir . DIRECTORY_SEPARATOR . $field;

        if (!empty($image['title'])) {
            $file_name = preg_replace('/\.[^\.]+$/', '', $image['title']);
        } else {
            $file_name = uniqid();
        }

        $file_path = $dir_path . DIRECTORY_SEPARATOR . $file_name . '.' . $file_extension;
        $i = 1;

        while (Storage::disk(config('uploadable.disk'))->exists($file_path)) {
            $file_path = $dir_path . DIRECTORY_SEPARATOR . $file_name . " ({$i})" . '.' . $file_extension;
            $i++;
        }
        Storage::disk(config('uploadable.disk'))->put($file_path, $img_data);
        return $file_path;
    }

    private function saveFile($file, $field, $file_data)
    {
        $file_data = str_replace(' ', '+', $file_data);
        $decode_file_data = base64_decode($file_data);

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $decode_file_data, FILEINFO_MIME_TYPE);

        try {
            if (isset(self::$mimes_type_to_extension[$mime_type])) {
                $file_extension = self::$mimes_type_to_extension[$mime_type];
            } else {
                throw new \Exception(trans('error.exceptions.invalid_mime_type'));
            }
        } catch (\Exception $e) {
            Log::warning($e->getMessage());

            return null;
        }

        $dir_path = config('uploadable.uploads_dir', 'uploads') . DIRECTORY_SEPARATOR
            . config('uploadable.files_dir', 'files') . DIRECTORY_SEPARATOR
            . $this->upload_dir . DIRECTORY_SEPARATOR . $field;

        if (!empty($file['title'])) {
            $file_name = preg_replace('/\.[^\.]+$/', '', $file['title']);
        } else {
            $file_name = uniqid();
        }

        $file_path = $dir_path . DIRECTORY_SEPARATOR . $file_name . '.' . $file_extension;
        $i = 1;

        while (Storage::disk(config('uploadable.disk'))->exists($file_path)) {
            $file_path = $dir_path . DIRECTORY_SEPARATOR . $file_name . " ({$i})" . '.' . $file_extension;
            $i++;
        }

        Storage::disk(config('uploadable.disk'))->put($file_path, $decode_file_data);

        return $file_path;
    }

    private function getImageByPath($image)
    {
        return $this->getFileByPath($image);
    }

    private function getFileByPath($file)
    {
        return [
            'src' => Storage::disk(config('uploadable.disk'))->url($file),
            'path' => Storage::disk(config('uploadable.disk'))->path($file),
            'title' => basename($file)
        ];
    }
}
