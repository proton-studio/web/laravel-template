<?php

namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Builder;

trait Filterable {

    protected $filterableTranslationJoined = false;

    function scopeFiltered(Builder $query, $filters) {

        $joinTranslations = false;

        if (isset($filters['id'])) {
            $query->whereIn($this->table . '.id', $filters['id']);
        }

        if (isset($filters['q'])) {

            $query->where(function ($query) use ($filters, &$joinTranslations) {

                foreach ($this->filterable_by_kw as $field) {

                    if (is_array($field)) {
                        $field = join('.', $field);
                    }

                    if (isset($this->translatedAttributes) && in_array($field, $this->translatedAttributes)) {
                        $field = $this->table . '_translation.' . $field;
                        $joinTranslations = true;
                    }

                    $query->orWhere($field, 'like', '%' . $filters['q'] . '%');
                }
            });
        }

        if ($joinTranslations) {

            $this->filterableJoinTranslationTable($query);
        }
    }

    function scopeOrdered(Builder $query, $sortOrder) {

        $field = $sortOrder[0];
        $direction =  $sortOrder[1];

        if (isset($this->translatedAttributes) && in_array($field, $this->translatedAttributes)) {

            $this->filterableJoinTranslationTable($query);

            $query->orderBy($this->table . '_translation.' . $field, $direction);

        } else {

            $query->orderBy($this->table . '.' . $field, $direction);
        }
    }

    protected function filterableJoinTranslationTable(Builder $query) {

        if (!$this->filterableTranslationJoined) {

            $query->select($this->table . '.*');

            $query->join($this->table . '_translation', $this->getRelationKey(), '=', $this->table . '.id');

            $this->filterableTranslationJoined = true;
        }
    }
}
