<?php

namespace App;

use App\Traits\Models\Filterable;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    use Filterable {
        Filterable::scopeFiltered as scopeFilteredTrait;
    }

    protected $filterable_by_kw = ['name'];

    protected $appends = ['permissions_ids'];

    public function getPermissionsIdsAttribute()
    {
        return $this->permissions()->pluck('id');
    }
}
