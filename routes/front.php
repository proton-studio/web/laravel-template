<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'front',
    'namespace' => 'Api\Front',
    'middleware' => ['api']
], function () {

    // Route::get('/language', 'LanguageController@index')->name('api.getLanguages');
    // Route::get('/language/{code}', 'LanguageController@show')->name('api.getLanguage');
    // Route::get('/page/{slug}', 'PageController@getPageBySlug')->name('api.getPageBySlug');
    // Route::get('/page', 'PageController@getPageList')->name('api.getPageList');
    // Route::get('/bundle/{slug}', 'BundleController@getBundleBySlug')->name('api.getBundleBySlug');
    // Route::get('/settings', 'SettingsController@index')->name('api.getSettings');

    // Route::get('/news', 'NewsController@index')->name('api.front.getNews');
    // Route::get('/news/{news:slug}', 'NewsController@show')->name('api.front.getNewsItem');

    // Route::get('/service', 'ServiceController@index')->name('api.front.getServices');
    // Route::get('/service/{id}', 'ServiceController@show')->name('api.front.getServices');

    // Route::get('/strategy/{strategy:slug}', 'StrategyController@show')->name('api.front.getStrategy');

    // Route::get('/value/{value:slug}', 'ValueController@show')->name('api.front.getValue');

    // Route::get('/team', 'TeamController@index')->name('api.front.getTeam');

    // Route::post('/dataset_application', 'DatasetApplicationController@save')->name('api.front.saveDataset');

    // Route::get('/dataset', 'DatasetController@index')->name('api.front.getDataset');

    // Route::post('/service_application', 'ServiceApplicationController@save')->name('api.front.saveService');

    // Route::get('/skillsframework/questions', 'SkillsframeworkController@questions')->name('api.front.getQuestions');
    // Route::get('/skillsframework/level/{level:slug}', 'SkillsframeworkController@level');
    // Route::post('/skillsframework/check', 'SkillsframeworkController@check')->name('api.front.checkQuestions');

    // Route::post('/user_service_rating', 'UserServiceRatingController@save')->name('api.front.save');
});
