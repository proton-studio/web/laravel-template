<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Api\Admin',
    'middleware' => ['api', \App\Http\Middleware\ClearPagesCache::class]
], function () {

    Route::group([
           'middleware' => 'auth:sanctum'
    ], function () {
        // Route::apiResource('/user', 'UserController');
        // Route::apiResource('/role', 'RoleController');
        // Route::apiResource('/page', 'PageController');
        // Route::apiResource('/settings','SettingsController');
        // Route::apiResource('/language', 'LanguageController');
        // Route::apiResource('/news', 'NewsController');
        // Route::apiResource('/category', 'CategoryController');
        // Route::apiResource('/dataset', 'DatasetController');
        // Route::apiResource('/strategy', 'StrategyController');
        // Route::apiResource('/strategy_progress', 'StrategyProgressController');
        // Route::apiResource('/service', 'ServiceController');
        // Route::apiResource('/value', 'ValueController');
        // Route::apiResource('/team', 'TeamController');
        // Route::apiResource('/info', 'InfoController');
        // Route::apiResource('/dataset_application_status', 'DatasetApplicationStatusController');
        // Route::apiResource('/dataset_application', 'DatasetApplicationController');
        // Route::apiResource('/service_application', 'ServiceApplicationController');
        // Route::apiResource('/skillsframework_tag', 'SkillsframeworkTagController');
        // Route::apiResource('/skillsframework_course', 'SkillsframeworkCourseController');
        // Route::apiResource('/skillsframework_question', 'SkillsframeworkQuestionController');
        // Route::apiResource('/skillsframework_level', 'SkillsframeworkLevelController');
    });
});
